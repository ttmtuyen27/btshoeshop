import React, { Component } from "react";
import PopUpConfirm from "./PopUpConfirm";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phẩm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.cartList.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, 1);
                      }}
                      className="btn btn-success"
                    >
                      Tăng
                    </button>
                    <span className="mx-4">{item.soLuong}</span>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, -1);
                      }}
                      className="btn btn-secondary"
                    >
                      Giảm
                    </button>
                  </td>
                  <td>
                    <PopUpConfirm
                      handleDeleteItem={this.props.handleDeleteItem}
                      id={item.id}
                    >
                      <button className="btn btn-danger">Xóa</button>
                    </PopUpConfirm>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
