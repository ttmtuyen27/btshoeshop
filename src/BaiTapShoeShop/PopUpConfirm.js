import React, { Component } from "react";
import { Popconfirm, message } from "antd";
export default class PopUpConfirm extends Component {
  confirm = (e) => {
    message.success("Đã xóa sản phẩm khỏi giỏ hàng");
    {
      this.props.handleDeleteItem(this.props.id);
    }
  };
  cancel = (e) => {
    message.error("Hủy");
  };
  render() {
    return (
      <>
        <Popconfirm
          title="Bạn có chắc chắn xóa sản phẩm này khỏi giỏ hàng?"
          onConfirm={this.confirm}
          onCancel={this.cancel}
          okText="Đồng ý"
          cancelText="Hủy"
        >
          <a>{this.props.children}</a>
        </Popconfirm>
      </>
    );
  }
}
