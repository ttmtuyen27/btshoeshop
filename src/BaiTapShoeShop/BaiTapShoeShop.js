import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./DataShoeShop";
import PopUpConfirm from "./PopUpConfirm";
import ProductList from "./ProductList";

export default class BaiTapShoeShop extends Component {
  state = {
    dataShoeShop: dataShoeShop,
    cartList: [],
  };

  addItemToCart = (product) => {
    let cloneCart = [...this.state.cartList];
    let index = cloneCart.findIndex((item) => {
      return item.id == product.id;
    });
    if (index == -1) {
      let newProduct = { ...product, soLuong: 1 };
      cloneCart.push(newProduct);
    } else cloneCart[index].soLuong += 1;
    {
      this.setState({ cartList: cloneCart });
    }
  };

  handleDeleteItem = (id) => {
    let index = this.state.cartList.findIndex((item) => {
      return item.id == id;
    });
    let cloneCart = [...this.state.cartList];
    if (index != -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cartList: cloneCart });
  };

  handleTangGiamSoLuong = (id, giaTri) => {
    let index = this.state.cartList.findIndex((item) => {
      return item.id == id;
    });
    let cloneCart = [...this.state.cartList];
    if (index != -1) {
      cloneCart[index].soLuong += giaTri;
    }
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({ cartList: cloneCart });
  };

  render() {
    return (
      <div className="container">
        <ProductList
          dataShoeShop={this.state.dataShoeShop}
          addItemToCart={this.addItemToCart}
        />
        <h3 className="my-5">
          Số lượng sản phẩm trong giỏ hàng: {this.state.cartList.length}
        </h3>
        {this.state.cartList.length > 0 && (
          <Cart
            handleDeleteItem={this.handleDeleteItem}
            cartList={this.state.cartList}
            handleTangGiamSoLuong={this.handleTangGiamSoLuong}
          />
        )}
      </div>
    );
  }
}
